var clientesObtenidos;

function gestionClientes() {
let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
let request = new XMLHttpRequest();
request.onreadystatechange = function functionName() {
  if (this.readyState == 4 && this.status == 200) {
    //console.table(JSON.parse(request.responseText).value);
    clientesObtenidos = request.responseText;
    procesarClientes();
  }
}

request.open("GET",url,true);
request.send();

}

function procesarClientes() {

  var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONClientes.value[0].ProductName);
  var divTabla = divTablaClientes;
  divTabla.innerText ="";
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

tabla.classList.add("table");
tabla.classList.add("table-striped");
  for (var cliente of JSONClientes.value) {
    console.log(cliente)
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = cliente.ContactName;

    var columnaCompany = document.createElement("td");
    columnaCompany.innerText = cliente.CompanyName;

    var columnaCountry = document.createElement("td");
    var imgBandera = document.createElement("img");
    pais = cliente.Country == "UK"? "United-Kingdom":cliente.Country;
    imgBandera.src = urlBandera + pais + ".png"
    imgBandera.classList.add("flag")

    //columnaCountry.innerText = cliente.Country;
columnaCountry.appendChild(imgBandera);


    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCompany);
    nuevaFila.appendChild(columnaCountry);
    tbody.appendChild(nuevaFila);
      //console.log(cliente.ProductName)
  }
tabla.appendChild(tbody);
divTabla.appendChild(tabla);



}
