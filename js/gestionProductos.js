var productosObtenidos;

function gestionProductos() {
let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
let request = new XMLHttpRequest();
request.onreadystatechange = function functionName() {
  if (this.readyState == 4 && this.status == 200) {
    //console.table(JSON.parse(request.responseText).value);
    productosObtenidos = request.responseText;
    procesarProductos();
  }
}

request.open("GET",url,true);
request.send();

}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = divTablaProductos;
  divTabla.innerText ="";
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

tabla.classList.add("table");
tabla.classList.add("table-striped");
  for (var producto of JSONProductos.value) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = producto.ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = producto.UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = producto.UnitsInStock;


    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tbody.appendChild(nuevaFila);
      console.log(producto.ProductName)
  }
tabla.appendChild(tbody);
divTabla.appendChild(tabla);



}
